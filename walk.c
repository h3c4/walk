/*
 * walk (1)
 *
 * Originally written for Plan 9 from Bell Labs by Dan Cross.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdbool.h>

static const char SHORT_USAGE[] = "Usage: walk [OPT] [DIRECTORY ...]\n";

static const char HELP[] =
"Recursively walk the specified directories (or current directory, if none).\n\n"
"      -0, --null              separate filenames by a null character.\n"
"      -d n, --depth n         walk at most n directories levels below the specified directories.\n"
"      --help                  display this help and exit.\n";

static const char ASK_FOR_HELP[] = "Try 'walk --help' for more information.\n";

static char* mkname(char* name, const char* basename, const char* filename, bool nullt) {
    int l = strlen(basename) + 1 + strlen(filename) + 1; /* <basename>/<filename>\0 */
    if (name == NULL) {
        name = malloc(l);
        if (name == NULL) {
            fputs("walk: malloc exhausted memory.", stderr);
            exit(EXIT_FAILURE);
        }
    } else {
        char* nname = realloc(name, l);
        if (nname == NULL) {
            fputs("walk: realloc exhausted memory.", stderr);
            free(name);
            exit(EXIT_FAILURE);
        }
        name = nname;
    }
    /* NOTE: switch to strcpy if snprintf gets too slow */
    snprintf(name, l, "%s/%s", basename, filename);
    /* print name to console */
    if (nullt) {
        fputs(name, stdout);
        fputc(0, stdout);
    } else {
        puts(name);
    }
    return name;
}

static int mkdepth(int depth) {
    return depth == -1 ? depth : depth - 1;
}

static int walk(const char dir[], int depth, bool nullt){
    if (depth == 0) {
        return 0;
    }
    DIR* d = opendir(dir);
    if (!d) {
        if (errno == ENOTDIR) {
            return 2; /* not a directory */
        }
        /* perror(dir); */
        fprintf(stderr, "walk: %s: %s\n", dir, strerror(errno));
        return 1;
    } else {
        int r = 0;
        char* name = NULL;
        for (const struct dirent *f = readdir(d); f; f = readdir(d)) {
            if (strcmp(f->d_name, ".") == 0 || strcmp(f->d_name, "..") == 0) {
                continue;
            }
            name = mkname(name, dir, f->d_name, nullt);
            /* walk if a directory (or unknown) */
            if ((f->d_type == DT_DIR || f->d_type == DT_UNKNOWN) && walk(name, mkdepth(depth), nullt) == 1) {
                r = 1; /* only consider unknown errors from recursive calls */
            }
        }
        if (name != NULL) {
            free(name);
        }
        closedir(d);
        return r;
    }
}

int main(int argc, char** argv) {
    int depth = -1;
    bool nullt = false;

    int optind = 1;
    char* opt;
    while (optind < argc && (opt = argv[optind])[0] == '-') {
        if (opt[1] == 'h' || strcmp(opt, "--help") == 0) {
            fputs(SHORT_USAGE, stdout);
            fputs(HELP, stdout);
            return 0;
        } else if (opt[1] == '0' || strcmp(opt, "--null") == 0) {
            nullt = true;
        } else if (opt[1] == 'd' || strcmp(opt, "--depth") == 0) {
            char* end;
            int val = 0;
            if (optind + 1 < argc) {
                val = strtol(argv[++optind], &end, 10);
            }
            if (val <= 0 || argv[optind] == end || *end != '\0') {
                fputs("walk: -d expects a positive integer.\n", stderr);
                return 1;
            }
            depth = val;
        } else {
            fputs(ASK_FOR_HELP, stderr);
            return 1;
        }
        optind++;
    }
    /* update argv pointer and argc counter */
    argv += optind;
    argc -= optind;

    int r = 0;
    if (argc == 0) {
        r = walk(".", depth, nullt);
    } else {
        for (int i = 0; i < argc; i++) {
            r |= walk(argv[i], depth, nullt);
        }
    }
    return r;
}
